<?php
//Login title
function new_wp_login_title() {
		return "MY SOFT IT";
}
add_filter('login_headertitle', 'new_wp_login_title');
//login url
function new_wp_login_url() {
		return "http://my-softit.com";
}
add_filter('login_headerurl', 'new_wp_login_url');
//login form style
function new_custom_login_page_style() {
	    echo '
         <style type="text/css">
	        body.login	{ background-image: url('.get_bloginfo('template_directory').'/images/mysoftit-background.jpg) !important; background-size:cover;  }
			#loginform  { background-color: #00B5E4; }
			#loginform label {color: #fff ;}
			</style>
';
}
add_action('login_head', 'new_custom_login_page_style');
//footer
function wpse_edit_footer() {
    add_filter( 'admin_footer_text', 'msit_edit_text', 11 );
}

function msit_edit_text($content) {
    return " Design and Development by <a href='http://my-softit.com'><b style='color:red;'>MY SOFT IT</b></a>";
}

add_action( 'admin_init', 'wpse_edit_footer' );
//logo
add_action('admin_head', 'my_custom_logo');

function my_custom_logo() {
echo '
<style type="text/css">
#header-logo { background-image: url('.get_bloginfo('template_directory').'/images/wp-logo.png) !important; margin:0 auto }
</style>
';
}

//user logo
function no_wp_logo_admin_bar_remove() {        
    ?>
        <style type="text/css">
            #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon:before {
                content: url(<?php echo get_stylesheet_directory_uri(); ?>/images/wp-logo.png)   !important;
                top: 2px; width:30px; height:30px;
            }
            #wpadminbar #wp-admin-bar-wp-logo > a.ab-item {
                pointer-events: none;
                cursor: default;
            }   
        </style>
    <?php
}
add_action('wp_before_admin_bar_render', 'no_wp_logo_admin_bar_remove', 0);
//admin color scema
function set_default_admin_color($user_id) {
	$args = array(
		'ID' => $user_id,
		'admin_color' => 'midnight'
	);
	wp_update_user( $args );
}
add_action('user_register', 'set_default_admin_color');
//not showing user color scema
if ( !current_user_can('manage_options') )
remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );



// wp admin logo
function custom_login_logo() {
	echo '<style type="text/css">h1 a { background: url('.get_bloginfo('template_directory').'/images/wp-msit.png) 30% 30% no-repeat !important;height:137px !important; width:332px !important; }</style>';
}
add_action('login_head', 'custom_login_logo');